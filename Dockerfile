FROM    openjdk:8-jdk-alpine

COPY    . /src/

RUN apk --update add bash libstdc++ && \
    cd /src/ && \
    ./gradlew build && \
    mkdir /app && \
    cp /src/build/libs/* /app/ && \
    rm -rf /src && \
    apk del bash libstdc++ && \
    rm -rf /var/cache/apk/*

EXPOSE  3000


WORKDIR /app/
CMD     ["java", "-jar", "RunningService.jar"]
