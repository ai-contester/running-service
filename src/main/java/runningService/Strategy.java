package runningService;

import org.bson.types.Binary;

public class Strategy {

    public String id;
    public Binary executable;

    public Strategy(String id, Binary executable){
        this.id = id;
        this.executable = executable;
    }

    public String getId() {
        return id;
    }

    public Binary getExecutable() {
        return executable;
    }
}
