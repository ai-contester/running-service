package runningService;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;

public class Session {

    public String id;

    public String[] strategies;
    public String status;
    public Date date;
    public Object pedestal;
    public Object gameStory;

    public Session(String[] strategies, String status, Date date, Object pedestal, Object gameStory)
    {
        this.strategies = strategies;
        this.status = status;
        this.date = date;
        this.pedestal = pedestal;
        this.gameStory = gameStory;
    }

    @Override
    public String toString()
    {
        return String.format("{dateStart: %s, strategies: %d, status: %e }",date,status,strategies);
    }
}