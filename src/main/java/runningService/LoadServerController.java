package runningService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class LoadServerController {

    public void checkVersionServer() throws IOException {
        URL tags = new URL("https://gitlab.com/api/v3/projects/1927946/repository/tags");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(tags.openStream()));
        String inputLine;
        StringBuilder sb = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
            sb.append(inputLine);
        in.close();
        System.out.println(sb);
    }
}
