package runningService;

import java.io.IOException;
import java.util.Scanner;

import static java.lang.System.getenv;

public class EnvironmentVariable {

    public boolean checkVariable(String nameVarible) throws IOException {
        return getenv(nameVarible) != null;
    }

    public void setVariable(String key, String var)
    {
        Runtime rt = Runtime.getRuntime();
        try {
            rt.exec(new String[]{"SET",key + "="+var});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getVariable(String key) throws Exception
    {
        if (!checkVariable(key)) throw new Exception("Varible is null");
        return getenv(key).toString();
    }
}
