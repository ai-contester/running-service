package runningService;

import com.mongodb.util.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;

import static java.lang.String.valueOf;

@RestController
public class SessionsController {

    private EventGSListener eventGSListener;
    private RunServerController runServerController;
    private Session lastSession;

    @Value("${path.server}")
    String pathtoserver;

    @Value("${server.delay}")
    long delay;

    @Value("${gameserver.ip}")
    String ip;

    @Value("${path.strategy.template}")
    String pathstrategytemplates;

    private String javaPath;

    @Autowired
    private SessionRepository repositorySession;

    @Autowired
    private StrategyRepository repositoryStrategies;

    @RequestMapping(value = "/sessions", method = RequestMethod.POST)
    @ResponseStatus( HttpStatus.ACCEPTED )
    public SessionResponse response(@RequestBody String[] strategies) throws Exception {

        //THIS IS JAVA
        javaPath = "/java/strategy/build/libs/strategy.jar";

        if (pathstrategytemplates.trim().equals("")){
            javaPath = "/strategy.jar";
            pathstrategytemplates = System.getProperty("user.dir");
        }
        if (pathtoserver.trim().equals("")) {
            pathtoserver = System.getProperty("user.dir") + "/game-server.jar";
        }

        File jarStrategyTemplates = new File(pathstrategytemplates + javaPath);

        if (!jarStrategyTemplates.exists())
        {
            throw new FileNotFoundException("On set path " + pathstrategytemplates + " and full path "
            + pathstrategytemplates + javaPath + " jar strategy templates wasn't found");
        }

        Session session = new Session(strategies,"running",new Date(System.currentTimeMillis()),null,null);
        lastSession = session;
        repositorySession.insert(session);
        runServerController = new RunServerController();

        try {
            runServerController.runServer(pathtoserver,strategies.length);
            eventGSListener = new EventGSListener(runServerController,this);
            eventGSListener.waitEvent();
        } catch (IOException e) {
            e.printStackTrace();
            runServerController.killProcess();
            throw new FailedToStartSessionException();
        }

        ArrayList<String> arrayStrategies = new ArrayList<>();

        for (String strategyF : strategies)
        {
            Strategy strategy = repositoryStrategies.findOne(strategyF);
            if (strategy == null || strategy.getExecutable() == null) {
                throw new UnexistedStrategyIdException();
            }
            String tempDir = Files.createTempDirectory("AIC").toString();
            Files.createDirectories(FileSystems.getDefault().getPath(tempDir+"/com/belocraft/"));
            String pathToNewFile = tempDir+"/com/belocraft/MyStrategy.class";
            FileOutputStream outputStream = new FileOutputStream(pathToNewFile);
            outputStream.write(strategy.getExecutable().getData());
            outputStream.flush();
            arrayStrategies.add(pathToNewFile);

            File strategyFile = new File(pathToNewFile);
            if (!strategyFile.exists())
            {
                throw new FileNotFoundException("On temp dir " + tempDir + " and full path " + pathToNewFile +
                        " strategy class wasn't found");
            }
            runStrategy(tempDir);
        }

        return new SessionResponse(session.id);
    }

    private void runStrategy(String tempDir) throws Exception
    {

        long mill = System.currentTimeMillis();

        while (eventGSListener.getPort() == -1)
        {
            if (eventGSListener.wasError())
            {
                runServerController.killProcess();
                throw new Exception("Start up game server failed");
            }
            if ((System.currentTimeMillis() - mill) > delay) {
                runServerController.killProcess();
                throw new Exception("Timeout wait port from game server");
            }
            Thread.sleep(1);
        }

        int port = eventGSListener.getPort();

        ProcessBuilder processBuilder = new ProcessBuilder("java","-classpath",tempDir+":"
                + pathstrategytemplates + javaPath
                ,"com.belocraft.Main",ip, valueOf(port));
        Process process = processBuilder.start();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                BufferedReader bf = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String msg = "";
                try {
                    while ((msg = bf.readLine()) != null)
                    {
                        if (!msg.equals("null")) {
                            System.out.println("################################### Message from strategy: " + msg);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    process.destroy();
                    runServerController.killProcess();
                }
            }
        });

        thread.start();

        Thread threadError = new Thread(new Runnable() {
            @Override
            public void run() {
                BufferedReader bf = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String msg = "";
                try {
                    while ((msg = bf.readLine()) != null)
                    {
                        if (!msg.equals("null")) {
                            System.out.println("################################### Message from strategy: " + msg);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    process.destroy();                    
                }
            }
        });
        threadError.start();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Unexisted strategy id")
    class UnexistedStrategyIdException extends Exception
    {
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Failed to start game server")
    class FailedToStartSessionException extends Exception
    {
    }

    public void changeSessionStatusToComplete()
    {
        lastSession.status = "Complete";
        repositorySession.save(lastSession);
    }

    public void changeSessionStatusToError()
    {
        lastSession.status = "Error";
        repositorySession.save(lastSession);
    }

    public void saveLog(String json)
    {
        lastSession.gameStory = JSON.parse(json);
    }

    public void saveResult(String json)
    {
        lastSession.pedestal = JSON.parse(json);
        repositorySession.save(lastSession);
    }
}