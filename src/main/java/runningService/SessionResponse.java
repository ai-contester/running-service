package runningService;


public class SessionResponse {

    public String id;

    public SessionResponse(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
