package runningService;


import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;

public class RunServerController {

    private Process serverProcess;
    private BufferedReader bufferedReader;
    private BufferedReader errorReader;

    public void runServer(String pathtoserver, int quantityStrategies) throws IOException {

        if (!Files.exists(FileSystems.getDefault().getPath(pathtoserver)))
            throw new IOException("Executable game server not found");

        ProcessBuilder processBuilder = new ProcessBuilder("java","-jar",
                pathtoserver,String.valueOf(quantityStrategies));
        processBuilder.directory(new File("/"));
        serverProcess = processBuilder.start();
        bufferedReader = new BufferedReader(new InputStreamReader(serverProcess.getInputStream()));
        errorReader = new BufferedReader(new InputStreamReader(serverProcess.getErrorStream()));
    }

    public String readLineFromOutputStream() throws IOException {
        return bufferedReader.readLine();
    }

    public String readLineFromErrorStrem() throws IOException {
        return errorReader.readLine();
    }

    public void killProcess()
    {
        serverProcess.destroy();
    }
}
