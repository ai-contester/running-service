package runningService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;

public class EventGSListener {

    private RunServerController runServerController;
    private int port;
    private boolean serverError;
    private SessionsController sessionsController;

    public EventGSListener (RunServerController runServerController, SessionsController sessionsController)
    {
        this.runServerController = runServerController;
        this.port = -1;
        this.serverError = false;
        this.sessionsController = sessionsController;
    }

    public int getPort()
    {
        return  port;
    }
    public boolean wasError()
    {
        return serverError;
    }

    public void waitEvent()
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String input = "";
                while (true){
                    try {
                        input = runServerController.readLineFromOutputStream();
                        if (input == null) break;
                        event(input);
                        if (!input.equals("null")) {
                            System.out.println(
                                    String.format("################################### Message from server: %s", input));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();

        Thread threadError = new Thread(new Runnable() {
            @Override
            public void run() {
                String input = "";
                while (true){
                    try {
                        input = runServerController.readLineFromErrorStrem();
                        if (input == null) break;
                        if (!input.equals("null")) {
                            System.out.println(
                                    String.format("################################### Error server: %s", input));
                            serverError = true;
                            sessionsController.changeSessionStatusToError();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        threadError.start();
    }

    void event(String input)
    {
        if (input.contains("[PORT]"))
        {
            port = Integer.valueOf(input.split("]")[1].trim());
            System.out.println(String.format("################################### Current port is %s ",
                    port));
        }
        if (input.contains("[LOG]"))
        {
            sessionsController.saveLog(input.replace("[LOG]",""));
        }
        if (input.contains("[SCORE]"))
        {
            sessionsController.saveResult(input.replace("[SCORE]",""));
        }

        if (input.contains("[END]"))
        {
            sessionsController.changeSessionStatusToComplete();
        }
    }
}
