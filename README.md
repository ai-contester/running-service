# Running service
Running service used to run a start-up gaming sessions through post requests.

## Build
``` bash
gradle build
```

## Running
``` bash
gradle bootRun
```

## Configuration
Parameters set in environment varible or flags in console.
### Mongo URI
Required.
``` shell
mongo_uri
```
Example: mongodb://login:password@ip:port/database

Login and password is optional.

### Path to game server on your file system
Required.
``` shell
path_game_server
```
Example: C:/many/folders/game-server.jar

### Path to strategy template on your file system
Required.
``` shell
path_to_strategy_template
```
Example: C:/many/folders/strategy.jar

### Gameserver ip
Optional.
``` shell
gameserver_ip
```
Default host is 127.0.0.1

### Server port for spring
Optional.
``` shell
server_port
```
Default port is 80

### Time for connect strategys and game server in milliseconds
Optional.
``` shell
delay_server
```
Default time is 60000